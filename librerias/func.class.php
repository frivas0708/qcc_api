<?php

	DEFINE('SECRET', '1b3816dff1a9e9004d01fd0696eb8a5619fbd5b661e89d441cde1a649eb7ef11');

	class Funcs
	{

		public function token($length = 20)
		{
			$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ(){}[]:;,.#*-_+=/';
			$randomString = '';
			for ($i = 0; $i < $length; $i++) {
				$randomString .= $characters[rand(0, strlen($characters) - 1)];
			}
			return $randomString;
		}

		public function recurse_copy($src,$dst)
		{
			$dir = opendir($src);
			@mkdir($dst);
			while(false !== ( $file = readdir($dir)) ) {
				if (( $file != '.' ) && ( $file != '..' )) {
					if ( is_dir($src . '/' . $file) ) {
						recurse_copy($src . '/' . $file,$dst . '/' . $file);
					}
					else {
						copy($src . '/' . $file,$dst . '/' . $file);
					}
				}
			}
			closedir($dir);
		}

		public function Obj2Str($obj, $pos)
		{
			$initValue = "";
			$returnValue = "";

			foreach ($obj as $key => $value) {
				$initValue .= $value . "|";
			}

			$returnValue = substr($initValue, 0, strlen($initValue) - $pos);

			return $returnValue;
		}

		public function specialChars($unicodeChars = null)
		{
			$htmlChars = str_replace('u00e1', '&aacute;', $unicodeChars);
			$htmlChars = str_replace('u00e9', '&eacute;', $htmlChars);
			$htmlChars = str_replace('u00ed', '&iacute;', $htmlChars);
			$htmlChars = str_replace('u00f3', '&oacute;', $htmlChars);
			$htmlChars = str_replace('u00fa', '&uacute;', $htmlChars);
			$htmlChars = str_replace('u00c1', '&Aacute;', $htmlChars);
			$htmlChars = str_replace('u00c9', '&Eacute;', $htmlChars);
			$htmlChars = str_replace('u00cd', '&Iacute;', $htmlChars);
			$htmlChars = str_replace('u00d3', '&Oacute;', $htmlChars);
			$htmlChars = str_replace('u00da', '&Uacute;', $htmlChars);
			$htmlChars = str_replace('u00f1', '&ntilde;', $htmlChars);
			$htmlChars = str_replace('u00d1', '&Ntilde;', $htmlChars);
			$htmlChars = str_replace('u00bf', '&iquest;', $htmlChars);

			$htmlChars = str_replace('á', '&aacute;', $htmlChars);
			$htmlChars = str_replace('é', '&eacute;', $htmlChars);
			$htmlChars = str_replace('í', '&iacute;', $htmlChars);
			$htmlChars = str_replace('ó', '&oacute;', $htmlChars);
			$htmlChars = str_replace('ú', '&uacute;', $htmlChars);
			$htmlChars = str_replace('Á', '&Aacute;', $htmlChars);
			$htmlChars = str_replace('É', '&Eacute;', $htmlChars);
			$htmlChars = str_replace('Í', '&Iacute;', $htmlChars);
			$htmlChars = str_replace('Ó', '&Oacute;', $htmlChars);
			$htmlChars = str_replace('Ú', '&Uacute;', $htmlChars);
			$htmlChars = str_replace('ñ', '&ntilde;', $htmlChars);
			$htmlChars = str_replace('Ñ', '&Ntilde;', $htmlChars);
			$htmlChars = str_replace('¿', '&iquest;', $htmlChars);
			
			return $htmlChars;
		}

		public function decodeEntities($stringValue)
		{
			return html_entity_decode( $stringValue, ENT_QUOTES );
		}

		public function currDateTime($modify = null)
		{
			date_default_timezone_set('America/El_Salvador');
			$dt = new DateTime();
			if (is_null($modify))
			{
				return $dt->format('Y-m-d H:i:s');
			}
			else
			{
				$dt->modify($modify);
				return $dt->format('Y-m-d H:i:s');
			}
		}

		//Encrypt Function
		public function encrypt($encrypt)
		{
			$encrypt = serialize($encrypt);
			$iv = mcrypt_create_iv(mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC), MCRYPT_DEV_URANDOM);
			$key = pack('H*', SECRET);
			$mac = hash_hmac('sha256', $encrypt, substr(bin2hex($key), -32));
			$passcrypt = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, $key, $encrypt.$mac, MCRYPT_MODE_CBC, $iv);
			$encoded = base64_encode($passcrypt).'|'.base64_encode($iv);
			return $encoded;
		}
		 
		//Decrypt Function
		public function decrypt($decrypt)
		{
			$decrypt = explode('|', $decrypt.'|');
			$decoded = base64_decode($decrypt[0]);
			$iv = base64_decode($decrypt[1]);
			if(strlen($iv)!==mcrypt_get_iv_size(MCRYPT_RIJNDAEL_256, MCRYPT_MODE_CBC)){ return false; }
			$key = pack('H*', SECRET);
			$decrypted = trim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, $key, $decoded, MCRYPT_MODE_CBC, $iv));
			$mac = substr($decrypted, -64);
			$decrypted = substr($decrypted, 0, -64);
			$calcmac = hash_hmac('sha256', $decrypted, substr(bin2hex($key), -32));
			if($calcmac!==$mac){ return false; }
			$decrypted = unserialize($decrypted);
			return $decrypted;
		}

		public function session_start_timeout($timeout=0, $probability=100, $cookie_domain='/') {
			// Set the max lifetime
			ini_set("session.gc_maxlifetime", $timeout);
		 
			// Set the session cookie to timout
			ini_set("session.cookie_lifetime", $timeout);
		 
			// Change the save path. Sessions stored in teh same path
			// all share the same lifetime; the lowest lifetime will be
			// used for all. Therefore, for this to work, the session
			// must be stored in a directory where only sessions sharing
			// it's lifetime are. Best to just dynamically create on.
			$seperator = strstr(strtoupper(substr(PHP_OS, 0, 3)), "WIN") ? "\\" : "/";
			$path = ini_get("session.save_path") . $seperator . "session_" . $timeout . "sec";
			if(!file_exists($path)) {
				if(!mkdir($path, 600)) {
					trigger_error("Failed to create session save path directory '$path'. Check permissions.", E_USER_ERROR);
				}
			}
			ini_set("session.save_path", $path);
		 
			// Set the chance to trigger the garbage collection.
			ini_set("session.gc_probability", $probability);
			ini_set("session.gc_divisor", 100); // Should always be 100
		 
			// Start the session!
			session_start();
		 
			// Renew the time left until this session times out.
			// If you skip this, the session will time out based
			// on the time when it was created, rather than when
			// it was last used.
			if(isset($_COOKIE[session_name()])) {
				setcookie(session_name(), $_COOKIE[session_name()], time() + $timeout, $cookie_domain);
			}
		}

	}
	
?>