<?php
/**
 * Created by PhpStorm.
 * User: FRIVAS
 * Date: 02/12/2019
 */

class Constantes
{
	private $srv;
	private $usr;
	private $pwd;
	private $dbn;
	private $port;
	private $char;

	/**
	 * @srv string = nombre del servidor
	 * @usr string = usuario de la base de datos
	 * @pwd string = clave del usuario
	 * @dbn string = nombre de la base de datos
	 * @port int = puerto de conexion
	 * @char string = charset de la base
	 */
	public function __construct()
	{

		date_default_timezone_set('America/El_Salvador');
		$dt = new DateTime();

		$this->srv = 'localhost';
		$this->usr = 'qcc_usr';
		$this->pwd = '~5w23?4gzjb5';
		$this->dbn = 'qcc_db';
		//$this->port = '3306';
		$this->char = 'utf8';

		/*$this->srv = 'localhost';
		$this->usr = 'root';
		$this->pwd = 'MySQL2018Admin';
		$this->dbn = 'qcc';
		$this->port = '3306';
		$this->char = 'utf8';*/
	}

	/**
	 * @configuracion array = Datos para la conexion a la base de datos
	 */
	public function oConstantes()
	{
		$configuracion = Array(
			'servidor' => $this->srv,
			'usuario' => $this->usr,
			'clave' => $this->pwd,
			'base' => $this->dbn,
			'puerto' => $this->port,
			'charset' => $this->char
		);
		return $configuracion;
	}
}