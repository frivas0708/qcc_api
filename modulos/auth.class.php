<?php

session_start();
session_regenerate_id();

class Auth
{
	private $db;
	private $id;
	private $admin;
	private $active;
	private $table;
	private $data;
	private $notFound;
	private $success;
	private $fn;

	public function __construct($data = null, $id = null)
	{
		include("./librerias/MySQLiClass/MysqliDb.php");
		include("./librerias/constantes.class.php");
		include("./librerias/func.class.php");
		
		$constantes     = new Constantes();
		$const          = $constantes->oConstantes();
		$this->fn       = new Funcs();
		$this->db       = new MysqliDb($const['servidor'], $const['usuario'], $const['clave'], $const['base'], $const['puerto'], $const['charset']);
		$this->id       = 'id';
		$this->admin 	= 'idperfil';
		$this->active 	= 'activo';
		$this->table    = 'qcc_usuarios';
		$this->data     = $data;
		$this->notFound = 404;
		$this->success  = 200;
	}

	function verifyMethod($metodo, $ruta, $id=null){
		//Verifies what is the method sent.
		switch ($metodo) {
			case 'GET':
				return self::doGet($ruta, $id);
				break;
			case 'POST':
				return self::doPost($ruta, $id);
				break;
			default:
				//http_response_code(405);
				break;
		}
	}

	private function doGet($ruta)
	{
		$dataToSend = array();

		switch ($ruta[1])
		{
			case 'logout':

				$this->db->where($this->id, $ruta[2]);
				$this->db->update($this->table, array('token' => ''));

				// $logData = array(
				// 	"id_agent"				=> $ruta[2],
				// 	"id_state"				=> 2,
				// 	"change_status_date"	=> $this->fn->currDateTime(),
				// 	"description"			=> "Sesión Cerrada"
				// );
				// $resId	= $this->db->insert($this->logTable, $logData);

				// remove all session variables
				session_unset();
				// destroy the session
				session_destroy();

				http_response_code(200);
				header('Content-Type: application/json');
				echo json_encode(array('success' => true, 'status' => 200, 'msg' => 'Logged out'));

				break;

			default:
				http_response_code(400);
				header('Content-Type: application/json');
				echo json_encode(array('success' => false, 'status' => 400, 'msg' => 'Nothing Here'));
				break;
		}
	}

	private function doPost($ruta)
	{
		try
		{
			$jsonData	= file_get_contents('php://input');
			$data 		= json_decode($jsonData, TRUE);

			switch ($ruta[1])
			{
				case 'login':

					$response = array();
					$usr = $data['username'];
					$pwd = $data['password'];

					$this->db->where('usuario', $usr);
					$usuario = $this->db->get($this->table);

					if ($this->db->getLastErrno() === 0)
					{
						if (empty($usuario))
						{
							header('Content-Type: application/json');
							echo json_encode(array('success' => false, 
								'status' => 200, 'msg' => "No existe el usuario $usr" ));
						}
						else
						{
							$password = $usuario[0]['clave'];
							$tableToken = $usuario[0]['token'];
							foreach ($usuario as $item)
							{ 
								$item['clave'] = '************';
								array_push($response, $item);
							}

							$dec = $this->fn->decrypt($password);
							if ($dec != $pwd)
							{
								header('Content-Type: application/json');
								echo json_encode(array('success' => false, 'status' => 200, 'msg' => "Contraseña erronea" ));
							}
							else
							{
								if (!empty($usuario[0]['token']))
								{
									header('Content-Type: application/json');
									echo json_encode(array('success' => false, 'status' => 200, 'msg' => "Usuario <b>$usr</b> ya tiene una sesión abierta."));
								}
								else
								{
									$now = $this->fn->currDateTime();
									$tkn = array(
										"sess_userid" 	=> $response[0]['id'],
										"sess_username"	=> $response[0]['usuario'],
										"sess_date"		=> $now
									);

									$token = $this->fn->encrypt(json_encode($tkn));
									$_SESSION['sess_date']		= $token;

									// $logData = array(
									// 	"id_agent"				=> $response[0]['id'],
									// 	"id_state"				=> 1,
									// 	"change_status_date"	=> $now,
									// 	"description"			=> "Sesión Iniciada"
									// );
									
									// $resId	= $this->db->insert($this->logTable, $logData);

									$this->db->where($this->id, $response[0]['id']);
									$this->db->update($this->table, array('token'=>$token));

									// $this->db->where('id_agent', $response[0]['id']);
									// $this->db->where('DATE(change_status_date)', date("Y-m-d"));
									// $statelog = $this->db->get('sys_states_logs');

									//print_r($this->db->getLastQuery());
									//$response[0]['loginDate'] = $statelog[0]['change_status_date'];
									$response[0]['token'] = $token;
									header('Content-Type: application/json');
									echo json_encode(array('success' => true, 'status' => 200, 'msg' => "Inicio Correcto", 'data' => $response));
								}
							}
						}
					}
					else
					{
						http_response_code(400);
						header('Content-Type: application/json');
						echo json_encode(array('success' => false, 'status' => 400, 'msg' => $this->db->getLastError() ));
					}

					break;
				
				
			}
		}
		catch (Exception $e)
		{
			http_response_code(400);
			echo json_encode(array(
				'success'	=> false,
				'msg'		=> $e->getMessage()
			));
		}

	}

}
