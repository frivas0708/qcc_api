<?php

class Rubros
{
	private $db;
	private $id;
	private $tabla;
	private $data;
	private $notFound;
	private $success;
	private $fn;

	public function __construct($data = null, $id = null)
	{
		include("./librerias/MySQLiClass/MysqliDb.php");
		include("./librerias/constantes.class.php");
		include("./librerias/func.class.php");

		$constantes     = new Constantes();
		$const          = $constantes->oConstantes();
		$this->fn       = new Funcs();
		$this->db       = new MysqliDb($const['servidor'], $const['usuario'], $const['clave'], $const['base'], $const['puerto'], $const['charset']);
		$this->id       = 'id';
		$this->tabla    = 'qcc_rubros';
		$this->data     = $data;
		$this->notFound = 404;
		$this->success  = 200;
	}

	function verifyMethod($metodo, $ruta, $id=null){
		//Verifies what is the method sent.
		switch ($metodo) {
			case 'GET':
				return self::doGet($ruta, $id);
				break;
			case 'POST':
				return self::doPost($ruta, $id);
				break;
			default:
				//http_response_code(405);
				break;
		}
	}

	private function doGet($ruta)
	{
		switch ($ruta[1])
		{
			case 'mostrarTodos':
				$this->db->where('activo', 1);
				$data = $this->db->get($this->tabla);
				header('Content-Type: application/json');
				echo json_encode($data);
				break;
			case 'mostrarUno':
				$this->db->where($this->id, $ruta[2]);
				$data = $this->db->get($this->tabla);
				header('Content-Type: application/json');
				echo json_encode($data);
				break;
			default:
				http_response_code(400);
				header('Content-Type: application/json');
				echo json_encode(array('success' => false, 'status' => 400, 'msg' => $this->db->getLastError()));
				break;
		}
	}

	private function doPost($ruta)
	{
	    try
        {
            $jsonData	= file_get_contents('php://input');
            $data 		= json_decode($jsonData, TRUE);
            $id 		= $data[$this->id];

            unset($data[$this->id]);

            switch ($ruta[1])
            {
                case 'crear':

                    $data['fecha_creacion'] = $this->fn->currDateTime();
                    $data['activo'] = true;
                    unset($data['fecha_modificacion']);
                    unset($data['modificado_por']);
                    $resId	= $this->db->insert($this->tabla, $data);

                    if (!$resId)
                    {
                        http_response_code(400);
                        header('Content-Type: application/json');
                        echo json_encode(array('success' => false, 'status' => 400, 'msg' => $this->db->getLastError()));
                    }
                    else
                    {
                        $this->db->where($this->id, $resId);
                        $data = $this->db->get($this->tabla);
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }

                    break;

                case 'actualizar':

                    $data['fecha_modificacion'] = $this->fn->currDateTime();
                    unset($data['fecha_creacion']);
                    unset($data['creado_por']);

                    $this->db->where($this->id, $id);

                    if ($this->db->update($this->tabla, $data))
                    {
                        $this->db->where($this->id, $id);
                        $data = $this->db->get($this->tabla);
                        header('Content-Type: application/json');
                        echo json_encode($data);
                    }
                    else
                    {
                        http_response_code(400);
                        header('Content-Type: application/json');
                        echo json_encode(array('success' => false, 'status' => 400, 'msg' => $this->db->getLastError()));
                    }

                    break;
            }
        }
        catch (Exception $e)
        {
            http_response_code(400);
            echo json_encode(array(
                'success'	=> false,
                'msg'		=> $e->getMessage()
            ));
        }

	}

}
