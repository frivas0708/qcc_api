<?php
/**
* Created by PhpStorm.
* User: FRIVAS
* Date: 02/12/2019
*/

	include_once("libraries/cors.php");

	$ruta 	    = $_SERVER['REQUEST_URI'];
	$metodo     = $_SERVER['REQUEST_METHOD'];
	$datos       = json_decode(file_get_contents('php://input'), TRUE);
	$ruta      = substr($ruta, 1);
	$ruta      = explode("?", $ruta);
	$ruta      = explode("/", $ruta[0]);
	$ruta      = array_diff($ruta, array('API_Restful', 'qcc_api'));
	$ruta      = array_values($ruta);
	$arr_json   = null;

	if (count($ruta) >= 2 && count($ruta) <= 3)
	{
		try
		{
			include('modulos/'.$ruta[0].'.class.php');
			$clase = $ruta[0];
			$metodoClase = new $clase((isset($datos) ? $datos : null));
			$arr_json = $metodoClase->verifyMethod($metodo, $ruta);
		}
		catch (Exception $e)
		{
			http_response_code(400);
			echo json_encode(array(
				'success'	=> false,
				'msg'		=> $e->getMessage()
			));
		}
	}
	else
	{
		http_response_code(400);
		echo json_encode(array(
			'success'	=> false,
			'msg'		=> 'Bad Request'
		));
	}

	//http_response_code(404);
	//include('my_404.php'); // provide your own HTML for the error page
	//die();

	//header('Content-Type: application/json');
	//echo json_encode($arr_json);
